#! /usr/bin/env false

use v6.c;

class Version::Semantic
{
	has Int $.major;
	has Int $.minor;
	has Int $.patch;

	multi method new (
		Version:D $v,
    ) {
		self.new(|$v.parts);
    }

	multi method new (
		Str:D $v,
    ) {
		self.new(|$v.split(".")>>.Int);
    }

	multi method new (
		Int:D $major where -1 < * = 0,
		Int:D $minor where -1 < * = 0,
		Int:D $patch where -1 < * = 0,
    ) {
		self.bless(
			:$major,
			:$minor,
			:$patch,
        );
    }

	multi method new
	{
		die "Semantic::Version should be created with .new(Int, Int, Int), where each Int is 0 or higher";
	}

	method bump-major
	{
		$!major++;

		$!minor = 0;
		$!patch = 0;

		return self;
	}

	method bump-minor
	{
		$!minor++;

		$!patch = 0;

		return self;
	}

	method bump-patch
	{
		$!patch++;

		return self;
	}

	method parts
	{
		return ($!major, $!minor, $!patch);
	}

	method perl
	{
		return "Semantic::Version.new(\"{self.Str}\")";
	}

	method gist
	{
		return "Semantic::Version.new({self.Str})";
	}

	method Str
	{
		return "{$!major}.{$!minor}.{$!patch}";
	}
}

=begin pod

=NAME    Version::Semantic
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.1.0

=head1 SYNOPSIS

=item1 C<Version::Semantic.new>
=item1 C<Version::Semantic.new(v0)>

=head1 DESCRIPTION

A variant on C<Version> which enforces
L<Semantic Versioning|https://semver.org>.

=head1 EXAMPLES

=head2 Creating, bumping and saying

=begin input
use Version::Semantic;

my Version::Semantic $v .= new;

$v.bump-major;
$v.bump-patch;

say $v;
=end input

=begin output
1.0.1
=end output

=head2 Create from existing Version

=begin input
use Version::Semantic;

say Version::Semantic.new(v2.6.7);
=end input

=begin output
2.6.7
=end output

=head1 SEE ALSO

=item1 C<Version>

=end pod

# vim: ft=perl6 noet
