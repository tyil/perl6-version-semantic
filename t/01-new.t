#! /usr/bin/env perl6

use v6.c;

use Test;
use Version::Semantic;

plan 5;

is Version::Semantic.new(v1.9.2).perl, "Semantic::Version.new(\"1.9.2\")", "Initializes with Version";
is Version::Semantic.new("8.9.1").perl, "Semantic::Version.new(\"8.9.1\")", "Initializes with Str";
is Version::Semantic.new(7, 8, 1).perl, "Semantic::Version.new(\"7.8.1\")", "Initializes with Int, Int, Int";
is Version::Semantic.new.perl, "Semantic::Version.new(\"0.0.0\")", "Initializes without arguments";

dies-ok { dd Version::Semantic.new("1.3.3.7") }, "Does not initialize with incorrectly formatted Str";

# vim: ft=perl6 noet
