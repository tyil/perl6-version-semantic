#! /usr/bin/env perl6

use v6.c;

use Test;
use Version::Semantic;

my Version::Semantic $v .= new;

plan 8;

is $v.bump-major, "1.0.0", "bump-major on 0.0.0";
is $v.bump-minor, "1.1.0", "bump-minor on 1.0.0";
is $v.bump-major, "2.0.0", "bump-major on 1.1.0";
is $v.bump-patch, "2.0.1", "bump-patch on 2.0.0";
is $v.bump-major, "3.0.0", "bump-major on 2.0.1";
is $v.bump-minor, "3.1.0", "bump-minor on 3.0.0";
is $v.bump-patch, "3.1.1", "bump-patch on 3.1.0";
is $v.bump-major, "4.0.0", "bump-major on 3.1.1";

# vim: ft=perl6 noet
