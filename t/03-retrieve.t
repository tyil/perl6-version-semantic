#! /usr/bin/env perl6

use v6.c;

use Test;
use Version::Semantic;

my Version::Semantic $v .= new(2, 4, 6);

plan 5;

is $v.patch, 6, "Patch level correct";
is $v.minor, 4, "Minor level correct";
is $v.major, 2, "Major level correct";

is $v.parts, (2, 4, 6), "Parts are correct";
is ~$v, "2.4.6", "Stringifies correctly";

# vim: ft=perl6 noet
